package tk.trhcreations.thatonlineyardsale.structures;

import java.util.HashMap;

/**
 * Created by Adam Li on 12/4/2016.
 */

public class Sale {
    public HashMap<String, Boolean> itemList;
    public String ownerId;
    public String name;

    public Sale() {}

    public Sale(String ownerId, String name, HashMap<String, Boolean> itemList) {
        this.ownerId = ownerId;
        this.name = name;
        this.itemList = itemList;
    }

    public String getOwnerId(){
        return ownerId;
    }

    public String getName(){
        return name;
    }

    public HashMap<String,Boolean> getItemList(){
        return itemList;
    }
}
