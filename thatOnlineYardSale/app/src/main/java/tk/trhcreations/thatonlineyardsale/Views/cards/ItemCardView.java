package tk.trhcreations.thatonlineyardsale.views.cards;

import android.app.Fragment;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import tk.trhcreations.thatonlineyardsale.R;
import tk.trhcreations.thatonlineyardsale.adaptors.GeneralQueryAdapter;
import tk.trhcreations.thatonlineyardsale.views.custom.ProportionalImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ItemCardView.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ItemCardView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ItemCardView extends GeneralQueryAdapter.ViewHolder {

    public TextView itemName;
    public TextView itemInfo;
    public boolean starred = false;
    public ImageButton star;
    public ProportionalImageView previewImg;

    public ItemCardView(View v) {
        super(v);
        itemName = (TextView) v.findViewById(R.id.card_item_title);
        itemInfo = (TextView) v.findViewById(R.id.card_item_info);
        star = (ImageButton) v.findViewById(R.id.card_item_starred);
        previewImg = (ProportionalImageView) v.findViewById(R.id.card_item_image);
    }
}
