package tk.trhcreations.thatonlineyardsale.adaptors;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tk.trhcreations.thatonlineyardsale.R;
import tk.trhcreations.thatonlineyardsale.activities.MainActivity;
import tk.trhcreations.thatonlineyardsale.activities.SaleActivity;
import tk.trhcreations.thatonlineyardsale.structures.Sale;
import tk.trhcreations.thatonlineyardsale.views.cards.SaleCardView;

/**
 * Created by Adam Li on 12/4/2016.
 */

public class AllSaleQueryAdapter extends RecyclerView.Adapter<AllSaleQueryAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";
    private DatabaseReference databaseReference;
    private Context context;

    private Query databaseQuery;

    private List<String> saleID = new ArrayList<>();
    private List<Sale> saleList = new ArrayList<>();


    private ChildEventListener saleEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousId) {
            Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());

            Sale sale = dataSnapshot.getValue(Sale.class);

            //location of list item to change
            int loc = saleID.indexOf(previousId)+1;

            saleID.add(loc, dataSnapshot.getKey());
            saleList.add(loc, sale);
            notifyItemInserted(loc);
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousId) {
            Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());

            Sale sale = dataSnapshot.getValue(Sale.class);

            //location of list item to change
            int loc = saleID.indexOf(previousId)+1;

            saleID.remove(loc);
            saleID.add(loc, dataSnapshot.getKey());
            saleList.remove(loc);
            saleList.add(loc, sale);
            notifyItemChanged(loc);
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());

            //location of list item to change
            int loc = saleID.indexOf(dataSnapshot.getKey());

            saleID.remove(loc);
            saleList.remove(loc);
            notifyItemRemoved(loc);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousId) {
            Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());

            Sale sale = dataSnapshot.getValue(Sale.class);

            //location of list item to move
            int from = saleID.indexOf(dataSnapshot.getKey());
            int to = saleID.indexOf(previousId)+1;

            saleID.remove(from);
            saleID.add(to, dataSnapshot.getKey());
            saleList.remove(from);
            saleList.add(to, sale);
            notifyItemMoved(from, to);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.w(TAG, "onCancelled", databaseError.toException());
            Toast.makeText(context, "Failed to load.",
                    Toast.LENGTH_SHORT).show();
        }
    };

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    private class SaleCardView extends ViewHolder {

        TextView saleName;
        TextView saleInfo;
        TextView saleId;
        ImageButton star;
        GridView previewGrid;
        View view;

        SaleCardView(View v) {
            super(v);
            saleName = (TextView) v.findViewById(R.id.card_sale_title);
            saleInfo = (TextView) v.findViewById(R.id.card_sale_info);
            star = (ImageButton) v.findViewById(R.id.card_sale_starred);
            previewGrid = (GridView) v.findViewById(R.id.card_sale_grid);
            saleId = (TextView) v.findViewById(R.id.card_sale_id);
            view = v;
        }
    }

    public AllSaleQueryAdapter(DatabaseReference databaseReference, Context context) {
        this.context = context;
        this.databaseReference = databaseReference;
        this.databaseQuery = databaseReference.child("sales").orderByChild("name");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;

        //Sale Card
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_sale_card, viewGroup, false);

        return new SaleCardView(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        //Sale Card
        SaleCardView holder = (SaleCardView) viewHolder;
        holder.saleName.setText(saleList.get(position).name);
        holder.saleId.setText(saleID.get(position));
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SaleActivity.class);
                intent.putExtra("sale_id", ((TextView)v.findViewById(R.id.card_sale_id)).getText().toString());
                context.startActivity(intent);
            }
        });
        String infoText = (saleList.get(position).itemList != null ? saleList.get(position).itemList : new HashMap<String, Boolean>()).size() + " items";
        if (saleList.get(position).ownerId.equals((MainActivity.user != null ? MainActivity.user.getUid() : ""))){
            infoText = infoText + " | You own";
        }
        holder.saleInfo.setText(infoText);
    }

    @Override
    public int getItemCount() {
        return saleID.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        databaseQuery.addChildEventListener(saleEventListener);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView){
        databaseQuery.removeEventListener(saleEventListener);
    }

}