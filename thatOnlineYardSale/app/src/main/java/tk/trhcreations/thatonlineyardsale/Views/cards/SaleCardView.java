package tk.trhcreations.thatonlineyardsale.views.cards;

import android.app.Fragment;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import tk.trhcreations.thatonlineyardsale.R;
import tk.trhcreations.thatonlineyardsale.adaptors.AllSaleQueryAdapter;
import tk.trhcreations.thatonlineyardsale.adaptors.GeneralQueryAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SaleCardView.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SaleCardView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SaleCardView extends GeneralQueryAdapter.ViewHolder {

    public TextView saleName;
    public TextView saleInfo;
    public boolean starred = false;
    public ImageButton star;
    public GridView previewGrid;

    public SaleCardView(View v) {
        super(v);
        saleName = (TextView) v.findViewById(R.id.card_sale_title);
        saleInfo = (TextView) v.findViewById(R.id.card_sale_info);
        star = (ImageButton) v.findViewById(R.id.card_sale_starred);
        previewGrid = (GridView) v.findViewById(R.id.card_sale_grid);
    }
}
