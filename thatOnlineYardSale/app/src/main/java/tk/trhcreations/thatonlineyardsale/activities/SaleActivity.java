package tk.trhcreations.thatonlineyardsale.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import tk.trhcreations.thatonlineyardsale.R;
import tk.trhcreations.thatonlineyardsale.structures.Sale;

/**
 * Created by Adam Li on 12/2/2016.
 */

public class SaleActivity extends AppCompatActivity{

    private static final String TAG = "SaleActivity";
    private boolean saleOwner = false;
    private boolean saleMember = false;

    TextView title;
    TextView info;
    RecyclerView items;

    HashMap<String, Boolean> itemList;

    DatabaseReference databaseReference;

    ValueEventListener saleListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // Get Post object and use the values to update the UI
            Sale sale = dataSnapshot.getValue(Sale.class);
            title.setText(sale.name);
            itemList = (sale.itemList != null ? sale.itemList : new HashMap<String, Boolean>());
            String infoText = itemList.size() + " items";
            if (sale.ownerId.equals((MainActivity.user != null ? MainActivity.user.getUid() : ""))){
                saleOwner = true;
                infoText = infoText + " | You own";
            }
            info.setText(infoText);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            // Getting Post failed, log a message
            Log.w(TAG, "onCancelled", databaseError.toException());
            Toast.makeText(SaleActivity.this, "Failed to load.",
                    Toast.LENGTH_SHORT).show();
        }
    };

    private String saleId;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sale);

        saleId = getIntent().getStringExtra("sale_id");;

        databaseReference = FirebaseDatabase.getInstance().getReference();

        setTitle(R.string.view_sale);

        title = (TextView) findViewById(R.id.sale_title);
        info = (TextView) findViewById(R.id.sale_info);
        items = (RecyclerView) findViewById(R.id.sale_recycler_view);
    }

    @Override
    public void onResume(){
        super.onResume();
        databaseReference.child("sales").child(saleId).addValueEventListener(saleListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (saleOwner){
            getMenuInflater().inflate(R.menu.appbar_menu_add_delete, menu);
            return true;
        }
        if (saleMember) {
            getMenuInflater().inflate(R.menu.appbar_menu_add, menu);
            return true;
        }
        getMenuInflater().inflate(R.menu.appbar_menu, menu);
        return true;
    }

}
