package tk.trhcreations.thatonlineyardsale.views.cards;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import tk.trhcreations.thatonlineyardsale.R;
import tk.trhcreations.thatonlineyardsale.adaptors.GeneralQueryAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QuestionCardView.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QuestionCardView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuestionCardView extends GeneralQueryAdapter.ViewHolder {

    public ListView items;

    //temp
    public TextView title;

    public QuestionCardView(View v) {
        super(v);
        items = (ListView) v.findViewById(R.id.card_questions_list);
        title = (TextView) v.findViewById(R.id.card_questions_text);
    }
}
