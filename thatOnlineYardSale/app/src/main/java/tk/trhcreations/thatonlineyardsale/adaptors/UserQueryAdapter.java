package tk.trhcreations.thatonlineyardsale.adaptors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tk.trhcreations.thatonlineyardsale.R;
import tk.trhcreations.thatonlineyardsale.views.list.ObjectListView;

/**
 * Created by Adam Li on 12/4/2016.
 */

public class UserQueryAdapter extends RecyclerView.Adapter<UserQueryAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";

    private String[] mDataSet;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }


    public UserQueryAdapter(String[] mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public UserQueryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;
        //Sale Card
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_user_objects, viewGroup, false);

        return new ObjectListView(v);
    }

    @Override
    public void onBindViewHolder(UserQueryAdapter.ViewHolder viewHolder, final int position) {
        //Sale Card
        ObjectListView holder = (ObjectListView) viewHolder;
        holder.title.setText(mDataSet[position]);
    }

    @Override
    public int getItemCount() {
        return mDataSet.length;
    }
}