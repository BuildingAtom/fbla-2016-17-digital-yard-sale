package tk.trhcreations.thatonlineyardsale.views.custom;

/**
 * Created by Adam Li on 12/4/2016.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import tk.trhcreations.thatonlineyardsale.R;

public class ProportionalImageView extends ImageView {

    private int w = -1;
    private int h = -1;

    public ProportionalImageView(Context context) {
        super(context);
    }

    public ProportionalImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ProportionalImageView,
                0, 0);

        try {
            w = a.getInteger(R.styleable.ProportionalImageView_aspectWidth, -1);
            h = a.getInteger(R.styleable.ProportionalImageView_aspectHeight, -1);
        } finally {
            a.recycle();
        }
    }

    public ProportionalImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ProportionalImageView,
                0, 0);

        try {
            w = a.getInteger(R.styleable.ProportionalImageView_aspectWidth, -1);
            h = a.getInteger(R.styleable.ProportionalImageView_aspectHeight, -1);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (w != -1 && h != -1) {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = width * h / w;
            setMeasuredDimension(width, height);
        }
    }
}