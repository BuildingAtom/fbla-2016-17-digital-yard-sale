package tk.trhcreations.thatonlineyardsale.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import tk.trhcreations.thatonlineyardsale.R;
import tk.trhcreations.thatonlineyardsale.fragments.main.general.AllSalesFragment;
import tk.trhcreations.thatonlineyardsale.fragments.main.general.HomeFragment;
import tk.trhcreations.thatonlineyardsale.fragments.main.user.ItemsFragment;
import tk.trhcreations.thatonlineyardsale.fragments.main.user.SalesFragment;
import tk.trhcreations.thatonlineyardsale.fragments.main.user.StarFragment;
import tk.trhcreations.thatonlineyardsale.structures.Sale;

/**
 * Created by Adam Li on 12/2/2016.
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HomeFragment.OnFragmentInteractionListener, View.OnClickListener, StarFragment.OnFragmentInteractionListener, SalesFragment.OnFragmentInteractionListener, ItemsFragment.OnFragmentInteractionListener {

    private static final String TAG = "thatOYS_MAIN";
    private FirebaseAuth mAuth;
    public static FirebaseUser user;
    private FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseStorage storageInstance;
    private DatabaseReference mDatabase;

    private NavigationView navigationView;
    private View headerView;

    private boolean loggedIn = false;
    private String screenName = "";
    private int currentMenuItem = 0;
    private static final int RC_SIGN_IN = 9001;
    private StorageReference storageReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(R.string.home);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.getMenu().clear();
        navigationView.inflateMenu(R.menu.navigation_drawer);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(currentMenuItem).setChecked(true);
        headerView = navigationView.getHeaderView(0);
        headerView.findViewById(R.id.nav_account_area).setOnClickListener(this);


        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.content_fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            HomeFragment firstFragment = new HomeFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_fragment_container, firstFragment).commit();
        }

        //auth
        mAuth = FirebaseAuth.getInstance();
        storageInstance = FirebaseStorage.getInstance();
        storageReference = storageInstance.getReferenceFromUrl("gs://thatonlineyardsale-19829.appspot.com");

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                firebaseAuthStateChanged(firebaseAuth);
            }
        };

        mDatabase = FirebaseDatabase.getInstance().getReference();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (loggedIn && currentMenuItem < 4){
            getMenuInflater().inflate(R.menu.appbar_menu_add, menu);
            return true;
        }
        getMenuInflater().inflate(R.menu.appbar_menu, menu);

        return true;
    }

    public void firebaseAuthStateChanged (@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        MainActivity.user = user;

        invalidateOptionsMenu();
        if (user != null) {
            // User is signed in
            Log.d(TAG, "signed_in: " + user.getUid());
            Log.d(TAG, "signed_in_as: " + user.getDisplayName());
            Log.d(TAG, "profile_pic: " + user.getPhotoUrl());

            loggedIn = true;
            screenName = user.getDisplayName();
            // Set header view appropriately
            ((TextView) headerView.findViewById(R.id.nav_account_name)).setText(screenName);
            ImageView profilePic = (ImageView) headerView.findViewById(R.id.side_nav_bar_image);
            Glide.with(this).load(user.getPhotoUrl()).into(profilePic);

            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.navigation_drawer_user);
        } else {
            // User is signed out
            Log.d(TAG, "signed_out");
            loggedIn = false;
            ((TextView) headerView.findViewById(R.id.nav_account_name)).setText(R.string.nav_not_logged_in_text);
            headerView.setBackgroundResource(R.drawable.side_nav_bar);

            ((ImageView) headerView.findViewById(R.id.side_nav_bar_image)).setImageResource(android.R.color.transparent);

            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.navigation_drawer);
        }
        navigationView.getMenu().getItem(currentMenuItem).setChecked(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.add_sale_title_text)
                        .setView(getLayoutInflater().inflate(R.layout.dialog_add_sale, null))
                        // Set the action buttons
                        .setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                DatabaseReference sale = mDatabase.child("sales").push();
                                String saleID = sale.getKey();
                                sale.setValue(new Sale(user.getUid(), ((EditText) ((Dialog)dialog).findViewById(R.id.add_sale_dialog_name)).getText().toString(), null));
                                Map<String, Object> append = new HashMap<>();
                                append.put(saleID, true);

                                DatabaseReference d1 = mDatabase.child("users");
                                DatabaseReference d2 = d1.child(user.getUid());
                                DatabaseReference d3 = d2.child("ownsales");
                                d3.updateChildren(append);
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;

            case R.id.action_search:

                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Called when an item in the navigation menu is selected.
     *
     * @param item The selected item
     * @return true to display the item as the selected item
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        invalidateOptionsMenu();

        if (id == R.id.nav_home && currentMenuItem != 0) {
            setTitle(R.string.home);
            currentMenuItem = 0;
            // Create fragment and give it an argument specifying the article it should show
            HomeFragment newFragment = new HomeFragment();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.content_fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        } else if (id == R.id.nav_all && currentMenuItem != 1) {
            setTitle(R.string.all_sales);
            currentMenuItem = 1;
            // Create fragment and give it an argument specifying the article it should show
            AllSalesFragment newFragment = new AllSalesFragment();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.content_fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        } else if (id == R.id.nav_starred && currentMenuItem != 2) {
            setTitle(R.string.starred);
            currentMenuItem = 2;
            // Create fragment and give it an argument specifying the article it should show
            StarFragment newFragment = new StarFragment();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.content_fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        } else if (id == R.id.nav_sales && currentMenuItem != 3) {
            setTitle(R.string.sales);
            currentMenuItem = 3;
            // Create fragment and give it an argument specifying the article it should show
            SalesFragment newFragment = new SalesFragment();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.content_fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        } else if (id == R.id.nav_items && currentMenuItem != 4) {
            setTitle(R.string.items);
            currentMenuItem = 4;
            // Create fragment and give it an argument specifying the article it should show
            ItemsFragment newFragment = new ItemsFragment();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.content_fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        } else if (id == R.id.nav_logout) {
            item.setChecked(true);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage(R.string.dialog_log_out_message);

            // Add the buttons
            builder.setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    mAuth.signOut();
                    currentMenuItem = 0;
                    setTitle(R.string.home);
                    // Create fragment and give it an argument specifying the article it should show
                    HomeFragment newFragment = new HomeFragment();

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.content_fragment_container, newFragment);
                    transaction.addToBackStack(null);

                    // Commit the transaction
                    transaction.commit();
                    navigationView.getMenu().getItem(currentMenuItem).setChecked(true);

                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
                    drawer.closeDrawer(GravityCompat.START);
                }
            });
            builder.setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog, int id) {
                navigationView.getMenu().getItem(currentMenuItem).setChecked(true);

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
                drawer.closeDrawer(GravityCompat.START);}});

            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onRestart() {
        super.onRestart();
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nav_account_area:
                if (!loggedIn) {
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                                    .setTheme(R.style.AppTheme)
                                    .setIsSmartLockEnabled(false)
                                    .build(),
                            RC_SIGN_IN);
                }
                else {
                }
                break;
        }
    }
}
