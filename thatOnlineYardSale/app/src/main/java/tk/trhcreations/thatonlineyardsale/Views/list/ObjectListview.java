package tk.trhcreations.thatonlineyardsale.views.list;

import android.app.Fragment;
import android.view.View;
import android.widget.TextView;

import tk.trhcreations.thatonlineyardsale.R;
import tk.trhcreations.thatonlineyardsale.adaptors.UserQueryAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ObjectListView.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ObjectListView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ObjectListView extends UserQueryAdapter.ViewHolder {

    public TextView title;
    public TextView subTitle;

    public ObjectListView(View v) {
        super(v);
        title = (TextView) v.findViewById(R.id.list_title);
        subTitle = (TextView) v.findViewById(R.id.list_subtitle);
    }
}
