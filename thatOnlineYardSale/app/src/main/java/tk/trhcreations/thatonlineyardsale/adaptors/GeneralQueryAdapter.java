package tk.trhcreations.thatonlineyardsale.adaptors;

import android.app.DownloadManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;

import tk.trhcreations.thatonlineyardsale.R;
import tk.trhcreations.thatonlineyardsale.views.cards.ItemCardView;
import tk.trhcreations.thatonlineyardsale.views.cards.QuestionCardView;
import tk.trhcreations.thatonlineyardsale.views.cards.SaleCardView;

/**
 * Created by Adam Li on 12/4/2016.
 */

public class GeneralQueryAdapter extends RecyclerView.Adapter<GeneralQueryAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";

    private FirebaseDatabase firebaseDatabase;

    private long databaseCount = 0;
    private long datasetStartPos = 0;

    private static final int loadCount = 50;


    private String[] mDataSet;
    private int[] mDataSetTypes;

    private QueryWhat query;

    public static final int SALE = 0;
    public static final int QUESTION = 1;
    public static final int ITEM = 2;

    public enum QueryWhat{
        HOME, STARRED, Nearby
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }


    public GeneralQueryAdapter(String[] mDataSet, int[] mDataSetTypes) {
        this.mDataSet = mDataSet;
        this.mDataSetTypes = mDataSetTypes;
    }

    public GeneralQueryAdapter(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;
        //Question Card
        if (viewType == QUESTION) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.view_question_card, viewGroup, false);
            return new QuestionCardView(v);
        }

        //Item Card
        if (viewType == ITEM) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.view_item_card, viewGroup, false);
            return new ItemCardView(v);
        }

        //Sale Card
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_sale_card, viewGroup, false);

        return new SaleCardView(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        //Question Card
        if (viewHolder.getItemViewType() == QUESTION) {
            QuestionCardView holder = (QuestionCardView) viewHolder;
            holder.title.setText(mDataSet[position]);
            return;
        }

        //Item Card
        if (viewHolder.getItemViewType() == ITEM) {
            ItemCardView holder = (ItemCardView) viewHolder;
            holder.itemName.setText(mDataSet[position]);
            return;
        }

        //Sale Card
        SaleCardView holder = (SaleCardView) viewHolder;
        holder.saleName.setText(mDataSet[position]);
    }

    @Override
    public int getItemCount() {
        return mDataSet.length;
    }

    @Override
    public int getItemViewType(int position) {
        return mDataSetTypes[position];
    }
}